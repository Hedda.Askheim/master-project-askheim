# The function 'create_dicom' takes a npz file containing 1D arrays of yd values in voxels in a ROI.
# In the 1D-array the voxels are numbered from 1 or 0, to the number of voxels in your ROI.
# This array is put in a regular doseDICOM-file. 

# The npz-file contains arrays for each site size. The script will create DICOM files for all 
# the sites that it finds in the npz.file 

# ERROR help: This script will give an error if some of the yd-values among the voxels is NaN. 
#             The solution is to change all this nan-values to zero. Then it should run.
#             Example: In my case there were 0 NaN in the braistem file. In the CTV file there were 
#                      200 NaN values (which is only 0.03% of all yd values)

# The function create_dicom is based on script by Helge Henjum
 
import time
from scipy.sparse import load_npz # Load a sparse matrix from a file / save a sparse matrix to file
import scipy.sparse as sp
import pydicom as dicom
import os
import numpy as np

patient = str(input("\nEnter the patient label:"))
ROI = str(input("\nEnter the ROI:"))
field = str(input("\nEnter the field number:"))

# First find ROIdimensions:
datfile = str(input("\nEnter the path and filename to the datfile for the ROI:")) # Example: /path/Brainstem1_sb.dat
#Define some stuff before reading the ROI-files
ROI_cage_size = 0
ROI_voxel_list = []
ROI_bool_matrix= []
		
#Opens the first dat-file to define the dimension of the entire ROI-cage
ROI_dimensions = []
g = open(datfile,"r")
header = g.readline()
header = g.readline()

#Size of the ROI-cage,
ROI_cage_size = int((float(header.split()[0]))*(float(header.split()[1]))*(float(header.split()[2])))
bin_info = [(int(header.split()[0])),(int(header.split()[1])),(int(header.split()[2]))]
ROI_dimensions.append(bin_info)
header = g.readline()
min_table = [(float(header.split()[0])),(float(header.split()[1])),(float(header.split()[2]))]
ROI_dimensions.append(min_table)
header = g.readline()
max_table = [(float(header.split()[0])),(float(header.split()[1])),(float(header.split()[2]))]
ROI_dimensions.append(max_table)
print('ROI_dimensions:',ROI_dimensions)
g.close()

def ensure_csr_matrix(matrix):
	if sp.isspmatrix_csr(matrix):
	# If the matrix is already a CSR matrix, return it as is
		return matrix
	else:
	# Convert the matrix to a CSR matrix
		return sp.csr_matrix(matrix)
     
def create_dicom(DICOM_path, yd_values, label):
    
    # Defining lambda functions: 
    gridmin = lambda dp,iso,pxs: (dp-iso-pxs/2.0)/10.0
    gridmax = lambda xmin,db,pxs: xmin+(db*pxs)/10.0

    #Find RT dose and RT plan file from DICOM-folder
    for filename in os.listdir(DICOM_path):
        if ".dcm" in filename.lower():  # Check whether the file is DICOM
            d = dicom.read_file(DICOM_path+"/"+filename, force=True)
            if "SOPClassUID" in d:
                if d.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.8': # RTPlan
                    pf = dicom.read_file(DICOM_path+"/"+filename)

                elif d.SOPClassUID == '1.2.840.10008.5.1.4.1.1.481.2': # RT Dose
                    ds = dicom.read_file(DICOM_path+"/"+filename)

    xbins = int(ds.Columns)
    ybins = int(ds.Rows)
    zbins = int(ds.NumberOfFrames)
    print("xbin, ybins and zbins = ",xbins, ybins, zbins)
    dose_position = np.array(ds.ImagePositionPatient).astype(np.float16)
    pixel_size =  np.array(ds.PixelSpacing).astype(np.float16)
    isocenter_list = []
    for j in range(pf.FractionGroupSequence[0].NumberOfBeams):
        ibs = pf.IonBeamSequence[j]
        isocenter_list.append(ibs.IonControlPointSequence[0].IsocenterPosition)

    print('isocenter_list:',isocenter_list)

    slice_thickness = float(ds.GridFrameOffsetVector[1]-ds.GridFrameOffsetVector[0])
    x_min_dose = gridmin(dose_position[0],isocenter_list[0][0],pixel_size[0])
    x_max_dose = gridmax(x_min_dose,xbins,pixel_size[0])
    y_min_dose = gridmin(dose_position[1],isocenter_list[0][1],pixel_size[1])
    y_max_dose = gridmax(y_min_dose,ybins,pixel_size[1])
    z_min_dose = gridmin(dose_position[2],isocenter_list[0][2],slice_thickness)
    z_max_dose = gridmax(z_min_dose,zbins,slice_thickness)

    print('slice_thickness', slice_thickness)

    print('Printing x, y and z min dose:',x_min_dose, y_min_dose, z_min_dose)
    print('Printing x, y and z max dose:',x_max_dose, y_max_dose, z_max_dose)
    #
    #
    x_bin_size = (x_max_dose-x_min_dose)/xbins
    y_bin_size = (y_max_dose-y_min_dose)/ybins
    z_bin_size = (z_max_dose-z_min_dose)/zbins
    print('Printing x, y and z bin size',x_bin_size, y_bin_size, z_bin_size)

  
    x_bin_min = int((ROI_dimensions[1][0]-x_min_dose)/x_bin_size)
    y_bin_min = int((ROI_dimensions[1][1]-y_min_dose)/y_bin_size)
    z_bin_min = int((ROI_dimensions[1][2]-z_min_dose)/z_bin_size)

    print('Printing x, y and z bin_min',x_bin_min, y_bin_min, z_bin_min)

    print('ROI_dimensions[0][0] =',ROI_dimensions[0][0])

    x_bin_max = x_bin_min+ ROI_dimensions[0][0]
    y_bin_max = y_bin_min+ ROI_dimensions[0][1]
    z_bin_max = z_bin_min+ ROI_dimensions[0][2]
    print('Printing x, y and z bin_max',x_bin_max, y_bin_max, z_bin_max)


    dicom_dose  = np.zeros((xbins*ybins*zbins),dtype = "float32")
    opt_dose = yd_values 


    counter = 0

    for i in range(z_bin_min,z_bin_max):
        for j in range(y_bin_min,y_bin_max):
            for k in range(x_bin_min,x_bin_max):
                dicom_dose[k + j*xbins + i*ybins*xbins] = opt_dose[counter] 
                counter +=1
        
        
        print("dicom_dose:",dicom_dose)
        print("\nds.DoseGridScaling:")
        print(float(ds.DoseGridScaling))

        pixel_array = dicom_dose/(float(ds.DoseGridScaling))

        max_flk = float(np.amax(pixel_array))
        print("max_flk:",max_flk)
        #print (ds)
        max_tps = float(np.amax(ds.pixel_array))
        print("max_tps:",max_tps)

        # Need to scale the values of the pixel_array. This because in most cases
        # max value is 65535 due to 16 bit array elements. Therefore all values have
        # to be scaled accordingly so n_max,y_bin_max,z_bin_maxthat no values oveerides the max allowed value.
        # Eclipse Haukeland uses 32 bit
        #print(max_flk,max_tps)
        scale = float(max_flk/max_tps)

        pixel_array = pixel_array/scale
        # Define values to be integers

        pixel_array = np.rint(pixel_array)

        pixel_array = pixel_array.astype(int)
        #print (float(np.amax(ds.pixel_array)))
        # As a result, the DoseGridScaling must also be scaled
        ds.DoseGridScaling = str(round((float(ds.DoseGridScaling)*scale),14))
        #
        pix_arr = np.array(pixel_array, dtype ="uint32")
        #print (float(np.amax(ds.pixel_array)))
        ds.PixelData = pix_arr.tobytes()
        #print (float(np.amax(ds.pixel_array)))

        # Save the cretaed dicom file:
        ds.SeriesDescription = f"FLK {label}"
        ds.save_as(f"{save_path}/{label}.dcm")

  
    print ("Finished creating DICOM")


# Now we will create dicom:
DICOM_path = str(input("\nEnter the path to the DICOM files:")) # Input the path to DICOM files
save_path = str(input("\nEnter the path where you want to save the DICOM files:")) # Input the directory where you want to save the files you create in this script 
scoringfile_path = str(input("\nEnter the path to the npz file containing the yf and yd values:")) # Input

npz_comp = np.load(f'{scoringfile_path}/microdosimetry {patient} {ROI} field {field}.npz')

list_of_arrays = npz_comp.files # The npz contains one array for each site size
print('\nThe npz file you provided, contains the following arrays:')
print(list_of_arrays)

for f in list_of_arrays:  # Taking one site size at the time
     if 'yf' in f: 
          continue # We dont need to create dicom files for the yf-values. 
     yd_values = npz_comp[f]
         # Split the filename by underscore to separate the prefix and the number part
     parts = f.split('_')
     # Extract the number part and remove 'nm' from it
     site = int(parts[1].replace('nm', ''))
     
     label = f'FLK yd ({site} nm)-{patient} {ROI} field{field}' # Label/name of the final dicom file 
     print(f"\nCreating DICOM file {label}...")

     create_dicom(DICOM_path, yd_values, label)