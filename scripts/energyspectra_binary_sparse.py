# This is script to create energy spectra in all voxels 

import pandas as pd
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import csv
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from numpy import *
import numpy as np
import struct  
import scipy
import os
from datetime import datetime

# constants:
k = 1000
m = 1000000

# --------- INPUT ----------
NX = 29          # eventbin x
NY = 29          # eventbin y
NZ = 16          # eventbin z
patient= 'sb16'  # patient label
field = 1        # field number/name
spawn = 48       # the number of cores the simulation was splitted on
primaries = 10*m # the number of primaries in the simulation (per spawn, not total)
# path to the folder containing all scoring files form eventbin:
path_to_file = '/Home/siv32/has015/flukafiles/sb16_2/FLUKA_sb16_Plan6/Field_1/eventbin-filer fra cl1 - 17.04.24 (copy)/'
######################

voxels = NX*NY*NZ
print("The number of voxels is", voxels)
print("Primaries:",primaries)
print(f"Dimension is {(primaries* voxels)}")

# ------------- FUNCTIONS -------------------
def list_files(directory):
    files_22 = [] # eventbin 22 (fluence detector)
    files_23 = [] # eventbin 23 (fluence * energy detector)
    for filename in os.listdir(directory):
        if os.path.isfile(os.path.join(directory, filename)):
            _, file_extension = os.path.splitext(filename)
            if file_extension == '.22':
                files_22.append(filename)
            elif file_extension == '.23':
                files_23.append(filename)
    return files_22, files_23

# The function "read_binary_sparse" takes in a binary file from FLUKA eventbin detecotr and converts it to a practical sparse matrix
# INPUT: - 'filename' is the name of the binary file 
# 		 - primaries is the number of primaries in the simulation
#        - voxels is the number of voxels the eventbin has (the scoring grid dimension) 
# OUTPUT: - 'sparse_matrix' is a scipy sparse matrix in coo format containing all non-zero scorings from all primaries in all voxels for this file (this eventbin) 
def read_binary_sparse(filename, primaries, voxels):
	print(f'\nReading file {filename}')
	
	i = 222  # This is how many bytes there are until the first data
	
	ftnget = lambda type, file, i: (struct.unpack(type,file[i:i+4]),i+4) #Lambda function for easier bytes to datatype conversion 

	row_indices = []  # Row indices for sparse matrix
	col_indices = []  # Column indices for sparse matrix
	data = []  # Data values for sparse matrix

	with open(filename, mode='rb') as file: # Opens the file as binary
		fileContent = file.read() # Read the entire file
		
		############## READ EVENTS #################
		while i <len(fileContent): # The rest of the file is only events

			# Accessing info about the event, number,weighting etc, by lambda function
			nbx,i = ftnget('i',fileContent,i)        # Binning nr. (22 or 23) 
			ncase,i = ftnget('i',fileContent,i)      # Event nr.
			acase,i = ftnget('f',fileContent,i)      # Weight of primary
			mcase,i = ftnget('i',fileContent,i)      # mcase is always zero? 
			
			# Add 8 to byte-counter since it is a new line (8 bytes of something signals new line)
			i = i+8

			nhits,i = ftnget('i',fileContent,i)      # How many hits does the primary have

			#Loop through the list with all the hits for the primary
			for j in range(int(nhits[0])):
				if i + 4> len(fileContent):  # This was added to avoid error claiming that the number of bytes is not dividable by 4
					break					 # it might cause that we loose the last hit in the file, but if so, it is a drop in the ocean  
				vox_id,i = ftnget('i',fileContent,i) # VoxelID of the hit
				if i + 4> len(fileContent):  
					break
				hit,i = ftnget('f',fileContent,i) # Scoring (fluence or fluence times energy) in the voxel
				row_indices.append(ncase[0])      # Particle/event number as row index
				col_indices.append(int(vox_id[0])-1)  # Voxel ID as column index
				data.append(hit[0])  # Scoring value as data

			i+=8 # Add 8 to byte-counter since it is a new line (8 bytes of something signals new line)

	print('Length of row indices',len(row_indices))
	print('Length of column indices',len(col_indices))
	
	# Create sparse matrix
	sparse_matrix = scipy.sparse.coo_matrix((data, (row_indices, col_indices)), shape=(primaries+1, voxels))
	print('Shape of matrix:',sparse_matrix.shape)
	print(f"{(len(row_indices))/((primaries+1)*voxels)*100}% non-zero scorings")
	return sparse_matrix
###########################################

# -----------   CALCULATIONS ---------------

# List the files in the directory:
file_list_22, file_list_23 = list_files(path_to_file)
# Sort the lists alphabetically:
file_list_22 = sorted(file_list_22)
file_list_23 = sorted(file_list_23)
file_list_22 = file_list_22[:2]
file_list_23 = file_list_23[:2]
print("Files with .22 extension:", file_list_22)
print(f"There is {len(file_list_22)} fluence files")
print("\nFiles with .23 extension:", file_list_23)
print(f"There is {len(file_list_23)} fluenceE files")

fluence = read_binary_sparse(f'{path_to_file}/{file_list_22[0]}', primaries, voxels)
fluenceXenergy = read_binary_sparse(f'{path_to_file}/{file_list_23[0]}', primaries, voxels)

# Convert from coo to csr format to enable division 
#   (The coo format is great for assembling sparse matrices, but doesnt work for arithmetic operations)
fluence_csr = scipy.sparse.csr_matrix(fluence)
fluenceXenergy_csr = scipy.sparse.csr_matrix(fluenceXenergy)

# Divide fluenceXenergy on fluence in order to get the energy scorings:
assert fluence_csr.shape == fluenceXenergy_csr.shape, "Matrices must have the same shape"
# Calculate the inverse of each element in fluence_csr
inverse_fluence_csr = scipy.sparse.csr_matrix((1 / fluence_csr.data, fluence_csr.indices, fluence_csr.indptr), shape=fluence_csr.shape)
# Perform element-wise division
energy_scorings_csr = fluenceXenergy_csr.multiply(inverse_fluence_csr)
# Multiply all elements by 10^3 to change the unit from GeV to MeV
energy_scorings_csr = energy_scorings_csr.multiply(10**3) 

# Find mean and max energy among all scorings:
mean_energy = np.mean(energy_scorings_csr.data)
max_energy = np.max(energy_scorings_csr.data)
print("The max energy scored is", max_energy)
print("The mean energy among all the energy scoring in all voxels is",mean_energy)

# Explore the data structure of the csr-matrix
limit = 10 #How many voxels to explore?
for i, energyscorings_in_voxel_i in enumerate(energy_scorings_csr.T): # Iterate over the columns (voxels) of the sparse matrix
    if i<limit: # i is the voxel number 
        #print(energyscorings_in_voxel_i)  
                     # When we print energyscorings_in_voxel_i, we are printing all 
                     # energy scorings in voxel i and alongside with each energyscoring 
                     # there is a tuple telling us which primary this energyscoring belongs to.
                     # Each scoring is printed on a new line like: (0 , 'primary number') 'energy scoring'
                     #    (The zero is there beacuse we only have two variables (primary and voxel number))
                    
        num_scorings_in_voxel = energyscorings_in_voxel_i.getnnz() 
        print(f'I voxel nummer {i} er det {num_scorings_in_voxel} primaries av {primaries} som har en scoring')
        energies_in_voxel = energyscorings_in_voxel_i.toarray().flatten()   # Lager en liste med alle energy scorings
        energies_in_voxel = energies_in_voxel[energies_in_voxel != 0]       # Fjerner alle null fra listen

# BINNING THE DATA:
energy_bins = list(range(0,101))
# Initialize an empty DataFrame that will store the energy spectra for all voxels
df = pd.DataFrame()
# Iterate over the non-zero entries of the sparse matrix
for voxel_id in range(voxels):
    # Extract the non-zero entries (energy scorings) for the current voxel
    voxel_data = energy_scorings_csr[:, voxel_id].data
    
    # Bin the energy values to create the histogram for the current voxel
    values, bins = np.histogram(voxel_data.clip(min=1, max=100), bins=energy_bins)
    
    # Add the histogram for the current voxel to the DataFrame
    df[voxel_id] = values

timestamp = datetime.now().strftime("%d.%m.%Y-%H:%M")  # Generate timestamp

timestamp = datetime.now().strftime("%d.%m.%Y-%H:%M")  # Generate timestamp
filename = f'{path_to_file}/energyspec_{timestamp}_.csv'  # Construct filename with timestamp
df.to_csv(filename, index=True)  
print(f"Energyspectra saved to {path_to_file} as energyspectra_{timestamp}.csv")



