# Script for calculating the dose-mean lineal energy (yd) in each voxel 
# (these are also calculated: yf, f(y), d(y), yd(y))

#       To run this script, you need the 11 lookup tables and the 
#       energyspectra in the voxels you want to study 

#       The script should take as input: the energy spectra in each voxel (stored in one dataframe)
#       And output the yd value in each voxel for 11 different site sizes. 
#       --> One npz.file with 11 arrays with yd values. One for each site size. 

import pandas as pd
import numpy as np
import os
pd.options.display.max_columns = None
import warnings

# -------Constants/definitions -------
k = 1000
m = 1000000
# y-bins are based on the max y-value for the site size. (These max-values were calcualted from Espens data)
max_y = [637.2937289249999, 402.82518901274995, 645.0816685320001, 340.8147766422, 
         173.15611551945, 62.81716617697499, 67.98206305800001, 58.146588675000004, 
         58.3943480757, 57.03440041419749, 72.89914050932624]
sites = [10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000]
max_y_sites = {key: value for key, value in zip(sites, max_y)}
#--------------------------------------------------

# Prompt the user to input variables
#################################################
patient = str(input("\nEnter the patient label: "))
ROI = str(input("\nEnter the ROI (e.g., 'brainstem', 'CTV'): "))
field = str(input("\nEnter the field number: "))
spawn = int(input("\nHas the simulation been splitted on several cores? If yes, enter the spawn number. If not enter 1. \n(If the energy spectra are already summed, enter 1):"))
if spawn > 1: # There are several energyspectra files
    path_to_energyspectra = str(input("\nEnter the path for the energy spectra files: "))
else: # There is only one energyspcetra file
    energyspectra_file = str(input("\nEnter the path and filename for the energy spectra: "))
save_in_path = str(input("\nEnter the path where you want to save the output files: "))
path_lookup_tables = str(input("\nEnter the path to the lookup tables: "))
#################################################
print("\nValues entered:")
print(f"Patient: {patient}")
print(f"ROI: {ROI}")
print(f"Field: {field}")

# Defining functions:
###############################################
def list_files(directory):
    csv_files = []
    for filename in os.listdir(directory):
        csv_files.append(filename)
    return csv_files

# Function that creates list of ybins
# Which bins to use when binning lineal energies, depends on site sizeas each site 
# has a different max lineal energy. The binwidth is 0.477 for all sites.
def predefine_ybins(nm):
    # Go to the disctionary max_y_sites and find the max y-value for this site size:
    max_y = max_y_sites[nm]
    print("maxy is:", max_y)
    binwidth = 0.477
    ybins = np.arange(binwidth/2,max_y,binwidth).tolist()
    print(f"number of ybins are {len(ybins)}")
    return ybins

# Function that reads the energy spectra in voxels and returns the yF and yD-values for that voxel. 
#INPUT:
#       - nm is the site size [nm] that we want to study
#       - csvfile is a csv file (dataframe) with energy spectra from each voxel (each column is a energy spectra)
#OUTPUT: - return list of yD and yF in each voxel 
def get_fy_all_voxels(nm, csvfile):
    print(f"\nCalculating the yf and yd for {nm} nm site. May take some minutes...")
    binwidth = 0.477
    ybins = predefine_ybins(nm) # Defining the bins for y


    # Open the csv-file with the f(y)-spectra 1, 2,..., 100 MeV for this site size:
    lookup_table = pd.read_csv(f"{path_lookup_tables}/fy_{nm}nm.csv")

    # Read the csv-file containing the energy spectra into a pandas dataframe: 
    df= pd.read_csv(csvfile, index_col = 0) 
    df.index.names = ['Energy bin [MeV]']
    df.columns.names = ['Voxel ID'] # each column is a voxel
    #print("The binned energy data looks like this:")
    #display(df)

    number_of_voxels = len(df.columns)
    
    # Add a column that is the weight:
    for column_name, column_data in df.items():
        protoncount_voxel = column_data.sum() 
        df[column_name + "_Weight"] = column_data / protoncount_voxel # weight = number of protons in energy bin / total number of protons in the voxel

    # Now, use the lookup tables to estimate the lineal energies in the voxels:

    yf_list = [] 
    yd_list = [] 
    for voxel_ID in df.columns:
        L = [] # This will be the list of lists of weighted f(y)
        if "_Weight" in voxel_ID: # We only need the column containing the weight, not the proton count
            for energy_bin in df.index:

                # Finding the f(y) in the lookup table for the corresponding energy
                # and multiplying it with the weight:
                fy_weighted = lookup_table[f"{str(energy_bin)} MeV"]*df.at[energy_bin, f"{voxel_ID}"] 

                L.append(fy_weighted)
            fy_voxel = np.sum(L,0) # Summing all fy_weighted in the voxel. Summation is element-by-element

            # ----------------------- Finn y_F og y_D: ---------------------------
            integral_fy = 0
            yf = 0
            for i in range(len(fy_voxel)-1):
                fydy = fy_voxel[i]*binwidth
                integral_fy += fydy #f(y)*dy sum, skal være 1
                y_i = ybins[i] # lineal energy for bin i
                yf += y_i*fydy      #integral (y*f(y)dy)
            yf_list.append(yf)
            dy = [] # dose probability density function 
            yd = 0
            for i in range(len(fy_voxel)):
                y_i = ybins[i]
                fydy = fy_voxel[i]*binwidth

                dy.append((y_i*fy_voxel[i])/yf)
                yd += (y_i**2*fydy)/yf
            yd_list.append(yd)
            # ---------------------- Finn yd(y): --------------------------------
            yd_y = []
            yd = 0
            for i in range(len(dy)):
                y_i = ybins[i]
                yd_y.append(dy[i]*y_i)
                yd += y_i*dy[i]*binwidth  #integral y*d(y)*dy
        else: 
            continue

    df = df.iloc[:,:-number_of_voxels] # Remove the columns containing the weight. Dont need them anymore

    return yf_list, yd_list
###################################################

## If the simulation has been splitted, the files need to be summed, to have one energyspectra file
if spawn > 1: 
    all_energy_spectra = list_files(path_to_energyspectra)
    print(f"\nThere are {len(all_energy_spectra)} files with energyspectra. They will be summed to one file now...")

    df_list = []

    # Read the csv file into a data frame and append it to df_list
    for file in all_energy_spectra:
        file_path = os.path.join(path_to_energyspectra, file)
        df = pd.read_csv(file_path, index_col=0)
        df_list.append(df)
    
    # Initialize the sum data frame with the structure of one of the dataframes
    sum_df = df_list[0].copy()

    # Iterate through the rest og the dataframes and add them to sum_df
    for table in df_list[1:]:
        sum_df += table
    
    print("\nThe total energy spectra is now calculated and will be saved to the energy spectra path")
    
    # Save the sum data frame as a csv file
    name = f'Total energyspectra {patient} {ROI} {field}.csv'
    sum_df.to_csv(f'{path_to_energyspectra}/{name}', index = True)
    energyspectra_file = f'{path_to_energyspectra}/{name}'

## Calculate yf- and yd-values in the voxels for 11 site sizes:
print("\n\n\nWill now calculate the yf and yd values in the voxels from the energy spectra in each voxel")
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    yf_10nm, yd_10nm = get_fy_all_voxels(nm = 10, csvfile = energyspectra_file)
    yf_20nm, yd_20nm = get_fy_all_voxels(nm = 20, csvfile = energyspectra_file)
    yf_50nm, yd_50nm = get_fy_all_voxels(nm = 50, csvfile = energyspectra_file)
    yf_100nm, yd_100nm = get_fy_all_voxels(nm = 100, csvfile = energyspectra_file)
    yf_200nm, yd_200nm = get_fy_all_voxels(nm = 200, csvfile = energyspectra_file)
    yf_500nm, yd_500nm = get_fy_all_voxels(nm = 500, csvfile = energyspectra_file)
    yf_1000nm, yd_1000nm = get_fy_all_voxels(nm = 1000, csvfile = energyspectra_file)
    yf_2000nm, yd_2000nm = get_fy_all_voxels(nm = 2000, csvfile = energyspectra_file)
    yf_5000nm, yd_5000nm = get_fy_all_voxels(nm = 5000, csvfile = energyspectra_file)
    yf_10000nm, yd_10000nm = get_fy_all_voxels(nm = 10000, csvfile = energyspectra_file)
    yf_20000nm, yd_20000nm = get_fy_all_voxels(nm = 20000, csvfile = energyspectra_file)

print("\nAll calculations are complete. Now saving the lists of yf and yd to .npz...")

np.savez_compressed(f'{save_in_path}/microdosimetry {patient} {ROI} field {field}', yf_10nm =yf_10nm,yd_10nm=yd_10nm,  
                    yf_20nm =yf_20nm, yd_20nm=yd_20nm, yf_50nm =yf_50nm, yd_50nm=yd_50nm, yf_100nm =yf_100nm, yd_100nm=yd_100nm,
                    yf_200nm =yf_200nm, yd_200nm=yd_200nm, yf_500nm =yf_500nm, yd_500nm=yd_500nm, yf_1000nm =yf_1000nm, yd_1000nm=yd_1000nm,
                    yf_2000nm =yf_2000nm, yd_2000nm=yd_2000nm, yf_5000nm =yf_5000nm, yd_5000nm= yd_5000nm, 
                    yf_10000nm =yf_10000nm, yd_10000nm=yd_10000nm, yf_20000nm =yf_20000nm, yd_20000nm=yd_20000nm)


print(f"\nSave is complete! You can find your microdosimetric values in {save_in_path}")

